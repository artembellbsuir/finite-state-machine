﻿#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const int transitionsTable[][8] = {
	{0, 0, 0, 0, 0, 0, 0, 0}, // 0 - error
	{0, 2, 0, 0, 0, 0, 0, 0}, // 1 - INITIAL_STATE - wait for `def `
	{0, 0, 3, 0, 0, 0, 0, 0}, // 2 - read 1 letter
	{0, 0, 3, 3, 4, 0, 0, 0}, // 3 - read as many letters and digits as exist, or open parenthesis
	{0, 0, 5, 0, 0, 7, 0, 0}, // 4 - read letter (id of first arg) or close parenthesis 
	{0, 0, 5, 5, 0, 7, 0, 6}, // 5 - read letter or digit of arg name OR separator or close parenthesis
	{0, 0, 5, 0, 0, 0, 0, 0}, // 6 - read letter (first char of next arg)
	{0, 0, 0, 0, 0, 0, 8, 0}, // 7 - read colon
	{0, 0, 0, 0, 0, 0, 0, 0}  // 8 - read FINAL STATE

};

void appendChar(char *str, char c) {
	char* charStr = (char*)malloc(2);
	charStr[0] = c;
	charStr[1] = '\0';
	strcat_s(str, 100, charStr);
}

int getCharType(char* c, int* index) {
	const int transitionDef = 1,
		transitionLetter = 2,
		transitionDigit = 3,
		transitionOpenParen = 4,
		transitionCloseParen = 5,
		transitionColon = 6,
		transitionArgSeparator = 7,
		transitionErr = 0;

	
	int i = *index;
	char tempDef[100] = "";
	for (int k = 1; k <= 4; k++) {
		appendChar(tempDef, c[i]);
		i++;
	}
	if (strcmp(tempDef, "def ") == 0) {
		*index = *index + i - 1;
		return transitionDef;
	}

	i = *index;
	char tempSeparator[100] = "";
	for (int k = 1; k <= 2; k++) {
		appendChar(tempSeparator, c[i]);
		i++;
	}
	if (strcmp(tempSeparator, ", ") == 0) {
		*index = *index + i - 1;
		return transitionArgSeparator;
	}

	char ch = c[*index];
	if ((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z')) 
		return transitionLetter;
	if (ch >= '0' && ch <= '9')
		return transitionDigit;
	if (ch == ':') 
		return transitionColon;
	if (ch == '(')
		return transitionOpenParen;
	if (ch == ')')
		return transitionCloseParen;
	
	return transitionErr;
}

int checkInput(char* s) {
	const int initialState = 1;
	const int finalStates[9] = {0, 0, 0, 0, 0, 0, 0, 0, 1};

	int curState = initialState;

	int i = 0;
	while (s[i] != '\0') {
		curState = transitionsTable[curState][getCharType(s, &i)];
		i++;
	}

	return finalStates[curState];
}

int main()
{
	char input[100];

	while (1) {
		printf("Enter string:\n");
		gets(input);

		if (strcmp(input, "exit") == 0) break;

		if (checkInput(input)) {
			printf("OK\n");
		}
		else {
			printf("BAD STRING\n");
		}
	}
	return 0;
}
